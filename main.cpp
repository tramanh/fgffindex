#include "Poco/Net/HTTPServer.h"
#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPRequestHandlerFactory.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Net/HTMLForm.h"
#include "Poco/URI.h"
#include "Poco/StringTokenizer.h"
#include "Poco/String.h"
#include "Poco/SharedPtr.h"
#include "Poco/StreamCopier.h"
#include "Poco/HashMap.h"
#include "Poco/DynamicFactory.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTimeFormatter.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/Exception.h"
#include "Poco/ThreadPool.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"
#include "Poco/Util/HelpFormatter.h"
#include "Poco/Util/IntValidator.h"
#include "Poco/ConsoleChannel.h"
#include "Poco/Timestamp.h"
#include "Poco/DateTime.h"
#include "Poco/Exception.h"
#include "Poco/Data/Session.h"
#include "Poco/Data/SQLite/Connector.h"
#include <iostream>

using Poco::Net::ServerSocket;
using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServer;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPServerParams;
using Poco::URI;
using Poco::StreamCopier;
using Poco::Timestamp;
using Poco::HashMap;
using Poco::DateTimeFormatter;
using Poco::DateTimeFormat;
using Poco::ThreadPool;
using Poco::DynamicFactory;
using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;
using Poco::Util::OptionCallback;
using Poco::Util::HelpFormatter;
using Poco::Util::IntValidator;
using Poco::DateTime;
using Poco::Timestamp;
using Poco::StringTokenizer;
using Poco::cat;

using Poco::Data::Session;
using Poco::Data::Statement;

using namespace Poco::Data::Keywords;

class NotFoundHandler : public HTTPRequestHandler
{
    void handleRequest(HTTPServerRequest& request,
                       HTTPServerResponse& response)
    {
        std::string forohfor("404 Not Found.");
        response.setContentLength64(forohfor.length());
        response.send() << forohfor;
    }
};

class PingRequestHandler : public HTTPRequestHandler
{
    void handleRequest(HTTPServerRequest& request,
                       HTTPServerResponse& response)
    {
        Session session("SQLite", Application::instance().config()
            .getString("AppHTTPServer.database", "main.db"));
        // rebuild profile
        std::string URI = request.getURI();
        std::size_t param = URI.find_first_of('?');
        if(param!=std::string::npos) URI=URI.substr(++param);
        URI = "http://" + request.clientAddress().host().toString()
             + ":" + URI + "/";
        // count
        Statement count(session);
        int c;
        count << "SELECT count(id) FROM Endhost WHERE uri=?",
            into(c),
            use(URI),
            now;
        std::string pong("pong");
        if (c > 0)
        {
            // update
            DateTime cur;
            Statement update(session);
            try{
                update << "UPDATE Endhost set lastactive=? WHERE uri=?",
                use(cur),
                use(URI),
                now;
            }
            catch (Poco::Exception& exc)
            {
                pong += "\n" + exc.displayText();
            }
        }
        response.setContentLength64(pong.length());
        response.send() << pong;
    }
};

class UpdateRequestHandler : public HTTPRequestHandler
{
    void handleRequest(HTTPServerRequest& request,
                       HTTPServerResponse& response)
    {
        Session session("SQLite", Application::instance().config()
            .getString("AppHTTPServer.database", "main.db"));
        // rebuild profile
        std::string URI = request.getURI();
        std::size_t param = URI.find_first_of('?');
        if(param!=std::string::npos) URI=URI.substr(++param);
        URI = "http://" + request.clientAddress().host().toString()
             + ":" + URI + "/";
        // count
        DateTime cur;
        Statement inserteh(session);

        std::string pong("pong");

        try{
            // insert
            inserteh << "INSERT INTO Endhost(uri,lastactive) VALUES(:uri, :lastactive)",
            use(URI),
            use(cur),
            now;
        }
        catch (Poco::Exception& exc)
        {
            pong += "\n" + exc.displayText();
        }

        Statement count(session);
        int c;
        count << "SELECT id FROM Endhost WHERE uri=?",
            into(c),
            use(URI),
            now;
        Statement update(session);
        try{
            // update lastactive
            update << "UPDATE Endhost set lastactive=? WHERE id=?",
            use(cur),
            use(c),
            now;
        }
        catch (Poco::Exception& exc)
        {
            pong += "\n" + exc.displayText();
        }
        // remove old
        Statement deleteold(session);
        deleteold << "DELETE FROM ef WHERE eid=?",
            use(c),
            now;
        std::stringstream ss;
        StreamCopier::copyStream64(request.stream(), ss);
        std::string s = ss.str();
        StringTokenizer t1(s, "\n", 
            StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);
        std::vector<std::string> filename;
        std::vector<std::string> chksum;
        for (std::string e : t1)
        {
            StringTokenizer t2(e, "/");
            if (t2.count() > 1)
            {
                std::string f = *(t2.begin());
                std::string h = *(t2.begin() + 1);
                filename.push_back(f);
                chksum.push_back(h);
            }
        }
        try
        {
            Statement insert(session);
            insert << "INSERT INTO File(filename,chksum) VALUES(:filename, :chksum)",
            use(filename),
            use(chksum);
            insert.execute();
            pong += "\nDone;";
        }
        catch (Poco::Exception& exc)
        {
            pong += "\n" + exc.displayText();
        }
        // update file table
        Statement select(session);
        std::vector<std::string> fileidVec;
        try
        {
        select << "SELECT id FROM File WHERE filename=:filenameVec and chksum=:chksumVec",
            into(fileidVec),
            use(filename),
            use(chksum),
            now;
        }
        catch (Poco::Exception& exc)
        {
            pong += "\n" + exc.displayText();
        }
        // insert to ef
        std::vector<int> eidVec(fileidVec.size(), c);
        try
        {
            Statement insert2(session);
            insert2 << "INSERT INTO ef(fid,eid) VALUES(:fid, :eid)",
            use(fileidVec),
            use(eidVec);
            insert2.execute();
            pong += "\nDone;";
        }
        catch (Poco::Exception& exc)
        {
            pong += "\n" + exc.displayText();
        }
        response.setContentLength64(pong.length());
        response.send() << pong;
    }
};

class SearchRequestHandler : public HTTPRequestHandler
{
    void handleRequest(HTTPServerRequest& request,
                       HTTPServerResponse& response)
    {
        Session session("SQLite", Application::instance().config()
            .getString("AppHTTPServer.database", "main.db"));
        // rebuild profile
        std::string URI = request.getURI();
        std::size_t param = URI.find_first_of('=');
        if(param!=std::string::npos) URI=URI.substr(++param);
        // filename=
        std::string decoded;
        Poco::URI::decode(URI, decoded, true);
        decoded = "%" + decoded + "%";
        Statement select(session);
        std::vector<std::string> filenameVec;
        std::vector<std::string> chksumVec;
        std::vector<std::string> filelist;
        int c;
        
        select << "SELECT filename,chksum FROM File WHERE filename like ?",
            into(filenameVec),
            into(chksumVec),
            use(decoded),
            now;
        
        std::vector<std::string>::iterator itr1 = filenameVec.begin();
        std::vector<std::string>::iterator itr2 = chksumVec.begin();
        for (; itr1 != filenameVec.end(); ++itr1, ++itr2) 
        { 
            filelist.push_back(*(itr1) + "/" + *(itr2));
        } 
        std::string pong(cat(std::string("\n"), filelist.begin(), filelist.end()));
        response.setContentLength64(pong.length());
        response.send() << pong;
    }
};

class GetRequestHandler : public HTTPRequestHandler
{
    void handleRequest(HTTPServerRequest& request,
                       HTTPServerResponse& response)
    {
        Session session("SQLite", Application::instance().config()
            .getString("AppHTTPServer.database", "main.db"));
        // rebuild profile
        std::string URI = request.getURI();
        std::size_t param = URI.find_first_of('=');
        if(param!=std::string::npos) URI=URI.substr(++param);
        // filename=, f=
        std::string decoded;
        Poco::URI::decode(URI, decoded, true);
        int c;
        StringTokenizer d(decoded, "/");
        std::string f;
        std::string h;
        if (d.count() > 1)
        {
            f = *(d.begin());
            h = *(d.begin() + 1);
        }
        
        Statement select1(session);
        select1 << "SELECT id FROM File WHERE filename = ? AND chksum = ? LIMIT 1",
            into(c),
            use(f),
            use(h),
            now;
        std::cout << c << std::endl;
        Statement select2(session);
        std::vector<std::string> uriVec;
        std::string pong;
        try
        {
        select2 << "SELECT uri FROM Endhost e LEFT JOIN ef ON ef.eid = e.id "
                "WHERE ef.fid=? AND"
                "((julianday(datetime('now')) - julianday(e.lastactive)) *100000 < 360)",
            into(uriVec),
            use(c),
            now;
        
         }
        catch (Poco::Exception& exc)
        {
            pong += "\n" + exc.displayText();
        }
        pong += (cat(std::string("\n"), uriVec.begin(), uriVec.end()));
        response.setContentLength64(pong.length());
        response.send() << pong;
    }
};
// end of legit
class AppRequestHandlerFactory: public HTTPRequestHandlerFactory
{
public:
    AppRequestHandlerFactory(){
        _clsHandlers.registerClass<NotFoundHandler>("404");
        _clsHandlers.registerClass<UpdateRequestHandler>("update");
        _clsHandlers.registerClass<SearchRequestHandler>("search");
        _clsHandlers.registerClass<GetRequestHandler>("get");
        _clsHandlers.registerClass<PingRequestHandler>("ping");
    }

    ~AppRequestHandlerFactory()
    {
    }

    HTTPRequestHandler* getHandler(std::string& name)
    {
        if(!_clsHandlers.isClass(name))
            name = "404";
        return _clsHandlers.createInstance(name);
    }

    HTTPRequestHandler* createRequestHandler(
        const HTTPServerRequest& request)
    {
        Application& app = Application::instance();
        app.logger().information(request.getURI());
        // pre-processing URI
        std::string URI = request.getURI();
        for(;;)
        {
            if(URI.empty() || '/' != URI.front()) break;
            URI.erase(0, 1);
        }
        std::size_t param = URI.find_first_of('?');
        if(param!=std::string::npos) URI=URI.substr(0,param);
        if(URI.empty()) URI="index";
        return getHandler(URI);
    }
private:
    DynamicFactory<HTTPRequestHandler> _clsHandlers;
};

class AppHTTPServer: public Poco::Util::ServerApplication
{
public:
    AppHTTPServer(): _helpRequested(false)
    {
    }

    ~AppHTTPServer()
    {
    }

protected:
    void initialize(Application& self)
    {
        ServerApplication::initialize(self);
    }

    void uninitialize()
    {
        ServerApplication::uninitialize();
    }

    void defineOptions(OptionSet& options)
    {
        ServerApplication::defineOptions(options);
        options.addOption(
        Option("help", "h", "display argument help information")
            .required(false)
            .repeatable(false)
            .callback(OptionCallback<AppHTTPServer>(
                this, &AppHTTPServer::handleHelp)));
        options.addOption(
            Option("port", "p", "Listening port (default 8080)")
            .required(false)
            .repeatable(false)
            .argument("port")
            .validator(new IntValidator(1,65535))
            .binding("AppHTTPServer.port"));
        options.addOption(
            Option("database","db","Path for database (default: "
            "main.db")
            .required(false)
            .repeatable(false)
            .argument("dbfile")
            .binding("AppHTTPServer.database"));
        options.addOption(
            Option("load", "l", "Load config from properties file "
                    "(default : " + config().getString("application.name")
                    + ".properties)")
            .required(false)
            .repeatable(false)
            .argument("file")
            .callback(OptionCallback<AppHTTPServer>(
            this, &AppHTTPServer::handleLoadProps)));
    }

    void handleLoadProps(const std::string& name,
                        const std::string& value)
    {
        loadConfiguration(value);
    }

    void handleHelp(const std::string& name, 
                    const std::string& value)
    {
        HelpFormatter helpFormatter(options());
        helpFormatter.setCommand(commandName());
        helpFormatter.setUsage("OPTIONS");
        helpFormatter.setHeader(
            "A web server that act as Index server.");
        helpFormatter.format(std::cout);
        stopOptionsProcessing();
        _helpRequested = true;
    }

    int main(const std::vector<std::string>& args)
    {
        if (!_helpRequested)
        {
            int maxQueued = 1000;
            int maxThreads = 1000;
            unsigned short port = (unsigned short)
                config().getInt("AppHTTPServer.port", 8080);
            (*this).logger().information( "Port: " +  std::to_string(port));
            (*this).logger().information( "Database: " + config().getString("AppHTTPServer.database", "main.db"));
            ServerSocket serverSocket(port);
            Poco::Data::SQLite::Connector::registerConnector();

            Session session("SQLite", Application::instance().config()
                .getString("AppHTTPServer.database", "main.db"));
            session << 
R"(CREATE TABLE IF NOT EXISTS "ef" (
  "id" INTEGER CONSTRAINT "pk_ef" PRIMARY KEY AUTOINCREMENT,
  "eid" INTEGER NOT NULL,
  "fid" INTEGER NOT NULL,
  CONSTRAINT ee
  UNIQUE (eid, fid) ON CONFLICT IGNORE
);

CREATE TABLE IF NOT EXISTS "endhost" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "uri" TEXT NOT NULL,
  "lastactive" DATETIME NOT NULL,
  CONSTRAINT eh
  UNIQUE (uri) ON CONFLICT IGNORE
);

CREATE TABLE IF NOT EXISTS "file" (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT,
  "filename" TEXT NOT NULL,
  "chksum" TEXT NOT NULL,
  CONSTRAINT hh
  UNIQUE (filename, chksum) ON CONFLICT IGNORE
)
)", now;
            ThreadPool::defaultPool().addCapacity(maxThreads);
            HTTPServerParams* pParams = new HTTPServerParams;
            pParams->setMaxQueued(maxQueued);
            pParams->setMaxThreads(maxThreads);
            HTTPServer httpServer(new AppRequestHandlerFactory(), 
                serverSocket, pParams);
            httpServer.start();
            waitForTerminationRequest();
            httpServer.stop();
        }
        return Application::EXIT_OK;
    }

private:
    bool _helpRequested;
};

int main(int argc, char** argv)
{
    AppHTTPServer app;
    return app.run(argc, argv);
}
